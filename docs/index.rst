.. mdinclude:: ../README.md

.. toctree::
   :maxdepth: 2
   :caption: Contents:

.. automodule:: some_package.a_module
   :members:

Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
