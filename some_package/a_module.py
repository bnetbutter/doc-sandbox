"""module docstring"""

def function():
    """
    Function docstring
    """

class Class:
    """
    Class docstring

    Parameters
    --- 
    name
        A generic description of `name`
    """

    def __init__(self, name):
        """
        Init docstring

        Parameters
        --------
        name
            A generic description of `name`
        """
        

    def method(self, arg):
        """
        Method docstring

        Parameters
        ----------
        arg:
            A generic description `arg`
        """